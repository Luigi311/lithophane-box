#include <FastLED.h>

// Type of Leds
#define LEDS_TYPE WS2812B
// RGB order of the Led controller
#define ORDER GRB

// Pin the Leds are connected to for data
#define DATA_PIN 3
// Number of Leds that are connected
#define NUM_LEDS 43
// Amount of Leds that on the top of the box
#define TOP_LEDS 2
// Amount of Leds to skip
#define LED_STEPS 3

CRGB leds[NUM_LEDS];
void setup() {
  Serial.begin(9600);
  Serial.println("Setting up");
  
  FastLED.addLeds<LEDS_TYPE, DATA_PIN, ORDER>(leds, NUM_LEDS);

  int color[3] = {215,255,255};

  int i;
  delay(500);
  Serial.println("Turning on Wall");
  for (i = 0 ; i < NUM_LEDS-TOP_LEDS ; i=i+LED_STEPS) {
    leds[i].r = color[0];
    leds[i].g = color[1];
    leds[i].b = color[2];
  }
  
  Serial.println("Turning on Top");
  for (i = NUM_LEDS; i > NUM_LEDS-TOP_LEDS ; i--) {
    leds[i].r = color[0];
    leds[i].g = color[1];
    leds[i].b = color[2];
  }
  
  FastLED.show();
  Serial.println("Done");
}

void loop() { }
